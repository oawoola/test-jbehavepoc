package com.medic.utils;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.*;

import com.medic.pages.Homepage;


public class TestUtils {

	WebDriver Driver;
	Config Config;
	Homepage Homepage;
	String URL;
	
	@Test
	public void testGetDriver() {
		Config = new Config();
		Driver = Config.getDriver("Firefox");
		assertTrue("Its Firefox",Config.isFirefox());
	}

	
	@Test
	public void testCountryURL() {
		Config = new Config();
	    Homepage = new Homepage();
	    URL = Config.countryURL(Country.UK);
		assertEquals("http://www.medicanimal.com/", URL);
		System.out.print(URL);
	}	
	
}
