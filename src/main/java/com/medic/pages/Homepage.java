package com.medic.pages;

import org.openqa.selenium.*;
import java.util.*;
import java.io.*;
import com.medic.utils.*;


public class Homepage {
	WebDriver Driver;
	String MACountry;
	String countryURL;
	private Boolean pageIsLoaded;
	Country country;
	final static String PAGETITLE = "MedicAnimal.com";
	String basketName;
	Config config;
	
	
	public Homepage(){
		
	}
	
	
	public Homepage(WebDriver Driver){
		this.Driver = Driver;
		config = new Config();
	}
	
	
	public void load(String MACountry) throws Exception{
		this.MACountry = MACountry;
		
		countryURL = config.countryURL(Country.valueOf(MACountry));
		Driver.get(countryURL);
		
		basketName = Driver.findElement(By.id("viewCart")).getText();
			
		
		if (basketName.equals(config.getCountryBasketName(Country.valueOf(MACountry)))) {
			pageIsLoaded = true;
		}
		else {
			throw new Exception("Page expected not loaded");
		}
	}
	
	
	
	public Boolean isLoaded(){
		return pageIsLoaded;
	}
	
}
