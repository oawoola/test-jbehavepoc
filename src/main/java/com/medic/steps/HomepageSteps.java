package com.medic.steps;

import org.jbehave.core.annotations.*;
import com.medic.pages.*;
import org.openqa.selenium.*;
import static org.junit.Assert.*;
import com.medic.utils.*;


public class HomepageSteps {
	
	Homepage Homepage;
	WebDriver Driver;
	Config Config;
	
	
	
	@Given("a supported <browser>")
	public void driver(@Named("browser")String browser){
		Config = new Config();
		Driver = Config.getDriver(browser);
	}
	
	@When("I submit a URL for a <country>")
	public void enterCountry(@Named("country")String country) throws Exception{
		Homepage = new Homepage(Driver);
		Homepage.load(country);
	}
	
	@Then("confirm that the home page has loaded")
	public void pagehasLoaded()throws Exception{
		assertTrue(Homepage.isLoaded());
		Driver.close();
	}
	
	
	

}
