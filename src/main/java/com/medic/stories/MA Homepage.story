Narrative:
In order to verify that each country site can be loaded as a customer I will like to load the homepage for all supported countries

Scenario:
Load the UK site

Given a supported <browser>
When I submit a URL for a <country>
Then confirm that the home page has loaded

Examples:
|browser|country|
|Firefox|UK|
|Firefox|SPAIN|