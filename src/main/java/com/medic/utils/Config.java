package com.medic.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;

public class Config {

	String browser;
	WebDriver Driver;
	private Boolean isFirefox;
	Country country;
	String countryURL;
	String basketName;
	
	
	public WebDriver getDriver(String browser){
		this.browser = browser;
		
			if (browser.equals("Firefox")){
				Driver = new FirefoxDriver();
				isFirefox = true;
			}
		
		return Driver;
	}
	
	
	
	public String countryURL(Country country){
		Properties props;
		props = new Properties();
		this.country = country; 
		
		try{
			props.load(new FileInputStream("homepage.properties"));
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		
		switch (country){
		
		case UK: 
		 countryURL= props.getProperty("UKHOMEPAGE");
		break;
		
		
		case SPAIN:
		 countryURL= props.getProperty("SPAINHOMEPAGE");
		break;
		
		
	}
		return countryURL;
	
	} 
	
	
	public String getCountryBasketName(Country country){
		this.country = country;
		Properties props;
		props = new Properties();
		
		try{
			props.load(new FileInputStream("homepage.properties"));
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		switch (country){
		
		case UK:
			basketName = props.getProperty("UKBASKETLABEL");
		break;	

		
		case SPAIN:
			basketName = props.getProperty("SPANISHBASKETLABEL");
		break;
		
		}
		
		return basketName;
	}
	
	
	public Boolean isFirefox(){
		return isFirefox;
	}
}
