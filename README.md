This is the repository of the jBehave functional test proof of concept. It aims to show what we could acheive with a Behavourial Driven Development framwework, jBehave in this instance.


It uses selenium also and the example in the code loads the current MedicAnimal homepage for the UK and Spain. 

Due to time constraints and problems with a particular library I have not set up the code for use with maven or ant. All the dependencies needed to run the example are in the libs folder. I suggest you create a project in Eclipse and run the example 
by right-clicking HomepageConfig.java(/src/main/java/com/medic/steps ) and running it as a junit test case.

The example should load a local copy of Firefox.

I have added a copy of the reports I generated when I ran the tests if you are mainly interested in seeing what jBehave results look like. You will find the reports in /jbehave/view/ and /jbehave